import hashlib
import hmac
import json
import os
from binascii import hexlify

import win32com.client
from django.conf import settings


# User api key length
API_KEY_LENGTH = 32


def generate_api_key():
    """
    Generates user api key for mailer service.
    """
    key = hexlify(os.urandom(API_KEY_LENGTH))
    return key.decode()


def sha256_hmac(key: str, msg: str):
    """
    Calculates sha256 hash of key + string.
    """
    digest = hmac.new(key.encode(), msg=msg.encode(), digestmod=hashlib.sha256).hexdigest()
    return digest


def send_email(message):
    """
    Constructs email from Message instance and sends it via Outlook.
    """
    # geting outlook client instance
    obj = win32com.client.Dispatch("Outlook.Application")

    # creating new email instance
    olMailItem = 0x0
    newMail = obj.CreateItem(olMailItem)

    # constructing new email
    newMail.To = message.to
    newMail.CC = message.cc
    newMail.Subject = message.subject
    newMail.Body = message.body
    newMail.HTMLBody = message.htmlbody

    # sending email
    newMail.Send()
