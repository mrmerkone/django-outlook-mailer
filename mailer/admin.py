from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from mailer.models import Application, User


class InlineApplication(admin.TabularInline):
    model = Application
    extra = 0
    readonly_fields = ["api_key"]
    
class CustomAdmin(UserAdmin):
    inlines = [InlineApplication,]

admin.site.register(User, CustomAdmin)
