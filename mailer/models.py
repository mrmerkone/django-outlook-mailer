from django.contrib.auth.models import AbstractUser
from django.db import models

from mailer.utils import API_KEY_LENGTH, generate_api_key


class User(AbstractUser):
    """
    Django default User model extention.
    """
    pass


class Application(models.Model):
    """
    Application model. Stores applications which uses mail service.
    """
    user = models.ForeignKey(
        User,
        related_name='apps'
    )
    api_key = models.CharField(
        unique=True,
        max_length=API_KEY_LENGTH,
        default=generate_api_key
    )
    name = models.CharField(
        unique=True,
        max_length=128
    )

    def __str__(self):
        return self.name

class Message(models.Model):
    """
    Message model.
    """
    app = models.ForeignKey(
        Application,
        related_name='messages'
    )
    to = models.TextField(
        blank=True,
        default=''
    )
    cc = models.TextField(
        blank=True,
        default=''
    )
    subject = models.TextField(
        blank=True,
        default=''
    )
    body = models.TextField(
        blank=True,
        default=''
    )
    htmlbody = models.TextField(
        blank=True,
        default=''
    )
    sended_flg = models.BooleanField(
        default=False
    )
    created_dt = models.DateTimeField(
        auto_now_add=True
    )

    def __str__(self):
        return f"Message-{self.created_dt}"