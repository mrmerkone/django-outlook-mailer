# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-12-05 15:17
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mailer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('to', models.TextField(blank=True, null=True)),
                ('cc', models.TextField(blank=True, null=True)),
                ('subject', models.TextField(blank=True, null=True)),
                ('body', models.TextField(blank=True, null=True)),
                ('sended_flg', models.BooleanField(default=False)),
                ('created_dt', models.DateTimeField(auto_now_add=True)),
                ('app', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='messages', to='mailer.Application')),
            ],
        ),
    ]
