import json

from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from mailer.serializers import MailerRequestSerializer
from mailer import utils

# MIME type application/json
APPLICATION_JSON = 'application/json'


@csrf_exempt
@require_http_methods(["POST"])
def mailer_handler_view(request):
    """
    Mailer request handler. Accepts application/json MIME type.

    1. Checks with MailerRequestSerializer if JSON payload and SHA256 signature  is valid;
    2. Stores email for sending it via Outlook instance.
    """

    # check MIME type
    if request.content_type != APPLICATION_JSON:
        return JsonResponse({"detail" : "Accepted MIME type: application/json."}, status=400)
    
    # check JSON syntax
    try:
        request_data = json.load(request)
    except Exception as e:
        return JsonResponse({"detail" : "Payload is not valid JSON."}, status=400)

    # check JSON fields and signature
    serializer = MailerRequestSerializer(data=request_data)
    if not serializer.is_valid():
        return JsonResponse(serializer.errors, status=400)

    # store email for sending
    serializer.save()
    return JsonResponse({"detail" : "Accepted for sending!"})