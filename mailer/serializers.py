from rest_framework.serializers import CharField, Serializer, ValidationError

from mailer import utils
from mailer.models import Application, Message


class MailerRequestSerializer(Serializer):
    """
    Mail request serializer for json validation.
    """
    app_name = CharField(required=True)
    to = CharField(required=True)
    cc = CharField(required=False)
    subject = CharField(required=True)
    body = CharField(required=False)
    htmlbody = CharField(required=False)
    signature = CharField(required=True)

    def validate(self, data):
        """
        Validates signature of serialized request.
        """
        # get app by provided app_name
        try:
            app = Application.objects.get(name=data.get('app_name'))
        except Application.DoesNotExist:
            raise ValidationError('There is no registered app with provided name.')

        # concatenate message attributes
        msg = f"{data.get('to')}{data.get('cc', '')}{data.get('subject')}{data.get('body','')}{data.get('htmlbody','')}"

        # compute hmac api_key + message
        expected_signature = utils.sha256_hmac(app.api_key, msg)

        if expected_signature != data.get('signature'):
            raise ValidationError('Bad signature.')

        return data

    def create(self, validated_data):
        """
        Creates new Message object from validated_data.
        """
        # excluding signature
        validated_data.pop('signature')
        # adding app instance
        app_name = validated_data.pop('app_name')
        app = Application.objects.get(name=app_name)
        validated_data.update({"app":app})
        # creating new Message object
        return Message.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Updates Message object with validated_data values.
        """
        instance.to = validated_data.get('to', instance.to)
        instance.cc = validated_data.get('cc', instance.cc)
        instance.subject = validated_data.get('subject', instance.subject)
        instance.body = validated_data.get('body', instance.body)
        instance.htmlbody = validated_data.get('htmlbody', instance.htmlbody)
        instance.save()
        return instance
