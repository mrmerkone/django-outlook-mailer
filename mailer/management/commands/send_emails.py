# -*- coding: utf-8 -*-
import csv

from django.conf import settings
from django.core.management import BaseCommand, CommandError

from mailer.models import Message
from mailer import utils

class Command(BaseCommand):
    
    help = 'Sends Messages with sended_flg=False'
    
    def handle(self, *args, **options):
        self.send_emails()
        
    def send_emails(self):
        """
        Sends Messages with sended_flg=False.
        """
        # geting Messeges to send
        msgs = Message.objects.filter(sended_flg=False)
        msgs_count = msgs.count()

        if msgs_count == 0:
            return

        # sending Messages
        print(f"Sending {msgs_count} emails.")
        for item in msgs:
            utils.send_email(item)
            print(f"Sended {item}.")

        # udating sended Messages
        Message.objects.filter(sended_flg=False).update(sended_flg=True)
        print("Done.")