"""
Keep your local settings here.
"""

DEBUG = True

ALLOWED_HOSTS = ['10.36.7.7', '127.0.0.1']
