# Django mail service

Проект микросервиса рассылки почты.

## Dependencies
* Python 3.6.4
* Django 1.11.12
* Django REST Framework 3.8.1

## How to run
1. Инициализировать приложение. Для этого в корне проекта в консоли выполнить:
``` batch
helper init
```
2. Запустить Python сервер:
``` batch
python manage.py runserver <host>:<port>
```

## How it works

### Запрос на отправку email
После запуска приложения оно начинает принимать **POST** запросы на URL `http://<host>:<port>/api/mailer/`. В заголовека запроса обязательно должен присутствовать `Content-Type application/json` а тело запроса должно содержать JSON следующего формата:
``` JSON
{
    "app_name": "<имя приложения зарегестрированого в админке *ОБЯЗАТЕЛЬНО*>",
    "to": "<получатели email в формате Outlook'a *ОБЯЗАТЕЛЬНО*>",
    "cc": "<получатели копии email в формате Outlook'a *ОПЦИОНАЛЬНО*>",
    "subject": "<тема email *ОБЯЗАТЕЛЬНО*>",
    "body": "<тело email *ОПЦИОНАЛЬНО*>",
    "htmlbody": "<HTML тело email *ОПЦИОНАЛЬНО*>",
    "signature": "<хэш HMAC_SHA256(app_api_key, to|cc|subject|body|htmlbody) *ОБЯЗАТЕЛЬНО*>"
}
```
Если JSON валиден и сигнатура верна, полученное сообщение сохраняется в БД с флагом `sended_flg=False` для дальнейшей отправки. Чтобы получить ключ API необходимо в админке приложения создать пользователя и приложение которое будет его использовать.

### Команда отправки email
Для отправки сообщений возможно использовать команду:
``` batch
helper send_emails
```
 или
``` batch
python manage.py send_emails
```
Она отправляет через клиент Outlook все сообщения с флагом `sended_flg=False` и после отправки проставляет им `sended_flg=True`.
Для постоянной работы можно создать задачу в Crontab или Планировщике задач, которая будет переодически вызывать данную команду. 