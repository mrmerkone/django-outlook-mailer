@ECHO OFF

set pythonRun=python
set managePath="manage.py"
set manageRun=%pythonRun% %managePath%

IF "%1" == "init" (
    copy mailservice\settings_local.example.py mailservice\settings_local.py
    %pythonRun% -c "from django.core.management import utils; print('SECRET_KEY=\'{}\''.format(utils.get_random_secret_key()))" > mailservice/secret.py
    call %0 migrate
    %manageRun% createsuperuser
    GOTO:return
)

IF "%1" == "send_email" (
    %manageRun% send_emails
    GOTO:return
)

ECHO Usage:
ECHO %0 init
ECHO     Initializes Django application
ECHO %0 send_email
ECHO     Runs send_email app command

:return